const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser');

const server = express();


const { Schema } = mongoose;
const dataSchema = new Schema({
  a: "String",
  b: "String"
});
const Data = mongoose.model("Data", dataSchema);

// mongoose.connect("mongodb://mongo:27017/datos", { useNewUrlParser: true } );
mongoose.connect("mongodb://localhost:27017/datos", { useNewUrlParser: true } );

server.use(bodyParser.urlencoded({
  extended: true,
}));
server.use(bodyParser.json());

server.get("/", function (req, res) {
  res.send("hello from app on port 3333");
});

server.post("/data", function (req, res) {
  const postedData = new Data({
    a: req.body.a,
    b: req.body.b
  });

  postedData.save(function (err, result) {
    if (err) return res.status(400).send(err);
    return res.send(result);
  });
});

server.get("/data", function (req, res) {
  Data.find({}, function (err, result) {
    if (err) return res.status(400).send(err);
    return res.send(result);
  });
});

server.listen(3333, function () {
  console.log("Server is running on 3333")
});