const chai = require("chai");
const mongoose = require("mongoose");
const superTest = require("supertest");

const { expect } = chai;

const request = superTest("http://localhost:3333");

describe("test post and get of data ", () => {
  
  it("test", () => {
    expect(3).equal(1 + 2);
  });

  it("Should be able to post data", async () => {
    const res = await request.post("/data")
      .send({
        a: "lala",
        b: "lolo"
      })
      .expect(200);

    expect(res.body.a).to.equal("lala");
    expect(res.body.b).to.equal("lolo");
  });

  it("Should be able to get data", async () => {
    const res = await request.get("/data")
      .expect(200);

    expect(res.body).to.be.an("array");
    expect(res.body.length).to.not.equal(0);
  });


});
